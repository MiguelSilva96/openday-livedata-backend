package com.ppb.openday.livedata.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * This component implements the persistence layer of the application. The implementation is given, but if you are
 * curious visit:
 *      https://www.sqlite.org/index.html
 *      https://github.com/xerial/sqlite-jdbc
 *      https://docs.oracle.com/javase/tutorial/jdbc/basics/
 */
@Component
public class SqliteLivedataPersistence implements LivedataPersistence {

    private static final Logger logger = LoggerFactory.getLogger(SqliteLivedataPersistence.class);

    private Connection connection;

    private String table;

    public SqliteLivedataPersistence(@Value("${sqlite.file}") String filename,
                                     @Value("${sqlite.table}") String table) {

        try {
            this.connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s", filename));
            Statement createTable = this.connection.createStatement();
            createTable.executeUpdate(String.format("create table if not exists %s (eventid string primary key, context string)", table));
        } catch (Exception e) {
            logger.error("Error creating connection to SQLite db", e);
        }
        this.table = table;
    }

    @Override
    public String getEventContext(String eventId) throws RepositoryException {
        try {
            PreparedStatement statement = this.connection.prepareStatement(String.format("SELECT * FROM %s WHERE eventid = ?", this.table));
            statement.setString(1, eventId);
            ResultSet results = statement.executeQuery();
            if (results.next()) {
                return results.getString("context");
            }
            logger.error("Returning null context for eventId = {}", eventId);
            return null;
        } catch (SQLException e) {
            logger.error("Error while fetching sqlite context for eventId = {}", eventId, e);
            throw new RepositoryException();
        }
    }

    @Override
    public void saveEventContext(String eventId, String context) throws RepositoryException {
        try {
            PreparedStatement statement = this.connection.prepareStatement(String.format("insert or replace into %s (eventid, context) values (?, ?)", this.table));
            statement.setString(2, context);
            statement.setString(1, eventId);

            statement.executeUpdate();
            logger.info("Executed save context query for eventId={}", eventId);
        } catch (SQLException e) {
            logger.error("Error while saving sqlite context for eventId = {}", eventId, e);
            throw new RepositoryException();
        }


    }
}
