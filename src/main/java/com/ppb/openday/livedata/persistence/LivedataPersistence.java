package com.ppb.openday.livedata.persistence;


/**
 * Interface that abstracts the persistence of Livescores. This contract defines a method for saving Livescores for a
 * a certain eventId.
 */
public interface LivedataPersistence {

    String getEventContext(String eventId) throws RepositoryException;
    void saveEventContext(String eventId, String context) throws RepositoryException;
}
