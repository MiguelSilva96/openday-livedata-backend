package com.ppb.openday.livedata.domain;

public class FootballLivescores {

    private String eventId;
    private FootballTeam home;
    private FootballTeam away;

    public FootballLivescores() {

    }

    public FootballLivescores(String eventId, FootballTeam home, FootballTeam away) {
        this.eventId = eventId;
        this.home = home;
        this.away = away;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public FootballTeam getHome() {
        return home;
    }

    public void setHome(FootballTeam home) {
        this.home = home;
    }

    public FootballTeam getAway() {
        return away;
    }

    public void setAway(FootballTeam away) {
        this.away = away;
    }

}
