package com.ppb.openday.livedata.domain;

public class FootballTeam {


    private String name;
    private Integer score;

    public FootballTeam() {

    }

    public FootballTeam(String name, Integer score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

}
